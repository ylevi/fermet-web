# Fernet Web

## Getting started

Install all requirements using:

``` 
pip install -r requirements.txt 
```

## Usage
The server exposes multiple endpoints, that can generate a symmetric encryption key, and use it to both encrypt & decrypt content.

All endpoints are available on localhost:8000, and the only format accepted is JSON. 


## CLI
You have been provided with an interactive CLI as well (cli.py) to interact with the backend server. See the below instructions to learn how to use it.  


### Generate:
Is valid through localhost:8000/generate. Use the below syntax to generate a key and receive its co-responding index:
``` 
curl localhost:8000/generate
```
You may also use:
```
python cli.py generate
```


### Encrypt (POST):
Is valid through localhost:8000/encrypt. Use the below syntax, with a generated key index (from the previous step) to encrypt any message.
``` 
curl -i -H "Content-Type: application/json" -X POST -d '{"uuid":"<key_index>", "msg":"<message_to_encrypt>"}' localhost:8000/encrypt
```
You may also use:
```
python cli.py encrypt
```
Follow the instructions that you are prompted for.

### Decrypt (POST):
Is valid through localhost:8000/decrypt. Use the below syntax, with a generated key index and your encrypted message (from the /encrypt phase) to decrypt it back to its original content.
``` 
curl -i -H "Content-Type: application/json" -X POST -d '{"uuid":"<key_index>", "msg":"<encrypted_message>"}' localhost:8000/decrypt
```
You may also use:
```
python cli.py decrypt
```
Follow the instructions that you are prompted for.
