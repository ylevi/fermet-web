import sys
import requests

ARG_AMOUNT = 2
GEN, ENC, DEC = "generate", "encrypt", "decrypt"
INVALID_ARG_AMOUNT = "You provided an incorrect amount of arguments."
INVALID_COMMAND = "You provided an invalid command."
GEN_FAIL = "Failed to generate key."
ENC_DEC_FAIL = "Failed to process request.\nPlease validate your key and message are valid."
URL = "http://localhost:8000/"
VALID_RESP = 200


def generate(url: str) -> requests.get:
    """
    Calls /generate backend
    :param url: backend URL
    :return: Generated key index
    """
    gen_url = url + GEN
    return requests.get(gen_url)


def encrypt_decrypt(url: str, func: str) -> requests.post:
    """
    Asks for two parameters from the user (index, message) and posts them to /encrypt or /decrypt.
    :param url: backend URL
    :param func: string to describe whether to decrypt or encrypt
    :return: Generated encrypted message
    """
    key_index = input("What is the key index? ")
    message = input("What is the message? ")
    data = {
        "uuid": key_index,
        "msg": message
    }

    return requests.post(url + func, json=data)


def run_command(command: str, url: str) -> str:
    """
    Returns the output data from the requested command
    :param command: the function to run (generate, encrypt, decrypt)
    :param url: the web server URL
    :return: The function data output
    """
    if command == GEN:
        # Run Gen
        generated = generate(url)
        if generated.status_code == 200:
            return generated.text
        else:
            return GEN_FAIL
    elif command == ENC or command == DEC:
        # Run enc
        enc_dec_respo = encrypt_decrypt(url, command)
        if enc_dec_respo.status_code == 200:
            return enc_dec_respo.text
        else:
            return ENC_DEC_FAIL

    return ""


def argument_handler(arguments: list, url: str) -> str:
    """
    Returns the data from the users request, after validating arguments provided by him
    :param arguments: list of input argument through CLI
    :param url: the web server URL
    :return: the data from the user request
    """
    if len(arguments) != ARG_AMOUNT:
        return INVALID_ARG_AMOUNT
    else:
        command = sys.argv[1]
        if command not in (GEN, ENC, DEC):
            return INVALID_COMMAND

        return run_command(command, url)


if __name__ == '__main__':
    print(argument_handler(sys.argv, URL))
