from cryptography.fernet import Fernet
from uuid import uuid4, UUID
from utils import DECODER


class Encryptinator:
    @staticmethod
    def generate_keys() -> (bytes, UUID):
        return Fernet.generate_key(), uuid4()

    def __init__(self, uuid=None, key=None):
        if not uuid and not key:
            self.key, self.uuid = self.generate_keys()
        else:
            self.key = key
            self.uuid = uuid
        self.encryptor = Fernet(self.key)

    def __repr__(self):
        return f'UUID - {self.uuid}'

    def __str__(self):
        return f'{self.uuid}'

    def get_creds(self):
        return self.uuid, self.key

    def encrypt_message(self, message: str) -> bytes:
        try:
            encrypted_message = self.encryptor.encrypt(bytes(message, DECODER))
            return encrypted_message
        except Exception as exc:
            print(f"Could not encrypt message, check the below details:\n"
                  f"{type(exc).__name__}:{exc}")

    def decrypt_message(self, encrypted_message: bytes) -> bytes:
        try:
            decrypted_message = self.encryptor.decrypt(encrypted_message)
            return decrypted_message
        except Exception as exc:
            print(f"Could not decrypt message, check the below details:\n"
                  f"{type(exc).__name__}:{exc}")
