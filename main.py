from flask import Flask, request, abort
from encryptinator import *
from utils import *


def request_validator() -> dict:
    if not request.json:
        abort(400, description="JSON only accepted")
    else:
        valid_json = json_handler(request.json)
        if valid_json:
            abort(400, description=valid_json)
        else:
            return request.json


def key_validator() -> (Encryptinator, str):
    request_dict = request_validator()
    valid_uuid, msg = request_dict[KEY_NAME], request_dict[MSG]
    if uuid_validator(valid_uuid):
        connection = db_connect()
        real_key = get_key(connection, valid_uuid)
        if real_key:
            return Encryptinator(valid_uuid, real_key), msg
        else:
            abort(400, description=NO_UUID_MSG)
    else:
        abort(400, description=INVALID_UUID_MSG)


if __name__ == '__main__':
    app = Flask("Encryptinator")

    @app.route('/', methods=['GET'])
    def home():
        return '<h1>Encryptinator</h1> Alpha Testing'


    @app.route('/generate', methods=['GET'])
    def api_generate():
        encryptor = Encryptinator()
        connection = db_connect()
        insert_key(connection, encryptor.uuid, bytes_to_str(encryptor.key))
        return str(encryptor.uuid)


    @app.route('/encrypt', methods=['POST'])
    def api_encrypt():
        encryptor, msg = key_validator()
        return encryptor.encrypt_message(msg)


    @app.route('/decrypt', methods=['POST'])
    def api_decrypt():
        encryptor, msg = key_validator()
        return encryptor.decrypt_message(bytes(msg, DECODER))


    app.run(port=8000)
