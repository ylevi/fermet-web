import sqlite3
from uuid import UUID
import os

DECODER = "utf-8"
DB_TIMEOUT = 10
DB_NAME = "keys.db"
DB_PATH = os.getcwd() + "/" + DB_NAME
KEY_NAME, MSG = "uuid", "msg"
NO_UUID_MSG = "Key is not found in the database."
INVALID_UUID_MSG="Key index is not a UUID."


def bytes_to_str(c_bytes: bytes) -> str:
    return c_bytes.decode(DECODER)


def uuid_validator(val_uuid: str) -> bool:
    try:
        UUID(val_uuid)
        return True
    except ValueError:
        return False


def table_validator(connection: sqlite3.Connection):
    return connection.execute("SELECT name FROM sqlite_master WHERE type = 'table' AND name='keys';").fetchall()


def db_connect(path: str = DB_PATH) -> sqlite3.Connection:
    connection = sqlite3.connect(path, DB_TIMEOUT)
    if not table_validator(connection):
        connection.execute("CREATE TABLE IF NOT EXISTS keys(uuid TEXT PRIMARY KEY, e_key TEXT);")
    return connection


def get_key(connection: sqlite3.Connection, uuid: str) -> str:
    query = f"SELECT e_key FROM keys where uuid='{uuid}'"
    output = [result for result in connection.execute(query)]
    # output will be an empty list if there is no key match
    if output:
        ''' We have to iterate through the output (aka cursor) to retrieve the single result.
            Then we select the first (and only key), and the result is a two cell tuple that 
            holds no value at the second cell, thus we grab the first cell, again.
        '''
        return output[0][0]
    else:
        # print("UUID doesn't exist in the database.")
        return ""


def insert_key(connection: sqlite3.Connection, uuid: str, e_key: str) -> str:
    if get_key(connection, uuid):
        raise ValueError("UUID already exists in the database!")
    else:
        result = connection.execute(f"INSERT INTO keys (uuid, e_key) VALUES ('{uuid}', '{e_key}');")
        connection.commit()

    return uuid


def json_handler(json) -> str:
    """
    JSON handler to verify JSON passed is only as meant, in the following structure:
    {"e_key":"uuid", "msg":"<message_content>"}
    :param json: a request.json
    :return: an error message or an empty string if it's valid
    """
    if len(json) != 2:
        return "Invalid argument amount."
    elif KEY_NAME not in json or MSG not in json:
        return "Invalid arguments."

    return ""
